import { Router } from 'express'
import { catchAsync } from '../middlewares/errors'
import actionsController from '../controllers/actionsController'

export default () => {
    const api = Router()

    api.get('/', catchAsync(actionsController.findAll))
    api.post('/', catchAsync(actionsController.create))

    api.get('/:id', catchAsync(actionsController.findOneById))
    api.put('/:id', catchAsync(actionsController.update))

    api.delete('/:id', catchAsync(actionsController.remove))

    return api
}
