import { Router } from 'express'
import { catchAsync } from '../middlewares/errors'
import logsController from '../controllers/logsController'

export default () => {
    const api = Router()

    api.get('/', catchAsync(logsController.findAll))

    return api
}
