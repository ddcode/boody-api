import { Router } from 'express'
import { catchAsync } from '../middlewares/errors'
import peopleController from '../controllers/peopleController'

export default () => {
    const api = Router()

    api.get('/', catchAsync(peopleController.findAll))
    api.post('/', catchAsync(peopleController.create))

    api.get('/:id', catchAsync(peopleController.findOneById))
    api.patch('/:id', catchAsync(peopleController.update))

    return api
}
