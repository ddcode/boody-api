import { Router } from 'express'
import { catchAsync } from '../middlewares/errors'
import devicesController from '../controllers/devicesController'

export default () => {
    const api = Router()

    api.get('/', catchAsync(devicesController.findAll))
    api.post('/', catchAsync(devicesController.create))

    api.get('/:id', catchAsync(devicesController.findOneById))
    api.put('/:id', catchAsync(devicesController.update))
    api.delete('/:id', catchAsync(devicesController.remove))

    return api
}
