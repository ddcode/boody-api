import { Router } from 'express'
import { catchAsync } from '../middlewares/errors'
import locationsController from '../controllers/locationsController'

export default () => {
    const api = Router()

    api.get('/', catchAsync(locationsController.findAll))
    api.post('/', catchAsync(locationsController.create))

    api.get('/:id', catchAsync(locationsController.findOneById))
    api.put('/:id', catchAsync(locationsController.update))
    api.delete('/:id', catchAsync(locationsController.remove))

    return api
}
