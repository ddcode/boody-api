import mongoose from 'mongoose'

const LogSchema = mongoose.Schema(
    {
        device: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Device',
        },
        action: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Action',
        },
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location',
        },
        details: {
            type: String,
            default: null,
        },
        time: {
            type: String,
            required: true,
            trim: true,
        },
    },
    {
        timestamps: true,
    }
)

const Log = mongoose.model('Log', LogSchema)
export default Log
