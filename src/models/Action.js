import mongoose from 'mongoose'

const ActionSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            unique: true,
        },
    },
    {
        timestamps: true,
    }
)

const Action = mongoose.model('Action', ActionSchema)
export default Action
