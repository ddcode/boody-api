import mongoose from 'mongoose'

const PeopleSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
        },
        photo: {
            type: String,
            required: true,
            trim: true,
        },
    },
    {
        timestamps: true,
    }
)

const People = mongoose.model('People', PeopleSchema)
export default People
