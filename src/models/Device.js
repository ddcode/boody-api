import mongoose from 'mongoose'

const DeviceSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            unique: true,
        },
        icon: {
            type: String,
            default: 'https://i.imgur.com/pKSVRVO.jpg',
            trim: true,
        },
        address: {
            type: String,
            required: true,
            trim: true,
        },
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location',
        },
        has_camera: {
            type: Boolean,
            default: false,
        },
        has_speaker: {
            type: Boolean,
            default: false,
        },
        has_thermometer: {
            type: Boolean,
            default: false,
        },
        has_movement_sensor: {
            type: Boolean,
            default: false,
        },
        send_notifications: {
            type: Boolean,
            default: false,
        },
    },
    {
        timestamps: true,
    }
)

const Device = mongoose.model('Device', DeviceSchema)
export default Device
