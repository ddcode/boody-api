import mongoose from 'mongoose'

const LocationSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            unique: true,
        },
    },
    {
        timestamps: true,
    }
)

const Location = mongoose.model('Location', LocationSchema)
export default Location
