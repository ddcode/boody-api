import mongoose from 'mongoose'

export default () => {
    // const credentials = `${process.env.DB_USER}:${process.env.DB_PASS}`
    const host = `${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`

    const dbURL = `mongodb://${host}`

    mongoose.connect(dbURL, { useNewUrlParser: true })
    mongoose.Promise = global.Promise
    mongoose.connection.on('error', err => {
        // eslint-disable-next-line no-console
        console.log(`Could not connect to the database ${dbURL}. Exiting now...`)
        process.exit()
    })
}
