import i18n from 'i18n'
import dotenv from 'dotenv'
import express from 'express'
import bodyParser from 'body-parser'
import jwt from 'jsonwebtoken'

import dbConfig from './configs/db'
import langConfig from './configs/lang'

// Routes
import people from './routes/people'
// import auth from './routes/auth'
import logs from './routes/logs'
import actions from './routes/actions'
import devices from './routes/devices'
import locations from './routes/locations'

// Helpers
import { verifyToken } from './helpers/Auth'

// Config
dotenv.config()

// APP Settings
const app = express()

// DB Config
dbConfig()

// JWT Auth
// const token = jwt.sign({ data: {} }, process.env.APP_SESSION_KEY)``

// i18n Config
langConfig()
app.use(i18n.init)

app.use(
    bodyParser.urlencoded({
        extended: false,
    })
)

app.use(bodyParser.json())

// Headers config
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE,PATCH')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
    next()
})

// Startup server
app.set('port', process.env.APP_PORT || 9999)

app.listen(app.get('port'), function() {
    // eslint-disable-next-line no-console
    console.log(i18n.__(`Server is up and running on port: `), app.get('port'))
})

app.get('/', verifyToken, (req, res) => {
    jwt.verify(req.token, process.env.APP_SESSION_KEY, (err, authData) => {
        if (err) {
            res.status(403).send({ message: i18n.__(`Forbidden`) })
        } else {
            res.json({
                authData,
            })
        }
    })
})

app.post('/', (req, res) => res.status(200).send(i18n.__(`Hello there`)))

// app.use('/auth', auth())
app.use('/people', people())
app.use('/logs', logs())
app.use('/actions', actions())
app.use('/devices', devices())
app.use('/locations', locations())
