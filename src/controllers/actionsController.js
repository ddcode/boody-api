import Action from '../models/Action'

export default {
    async findAll(req, res, next) {
        const data = await Action.find()

        return res.status(200).send(data)
    },

    async findOneById(req, res, next) {
        const { id } = req.params

        const data = await Action.findOne({
            _id: id,
        })
        if (!data) next()

        res.status(200).send(data)
    },

    async create(req, res, next) {
        const data = new Action(req.body).save()

        return res.status(200).send({ data, msg: `Action created` })
    },

    async update(req, res, next) {
        const { id } = req.params
        const { name } = req.body

        const data = await Action.findOne({ _id: id })

        data.name = name
        data.save()
        return res.status(200).send({ data, msg: `Action updated` })
    },

    async remove(req, res, next) {
        const { id } = req.params

        await Action.remove({ _id: id })
        const data = await Action.find()

        return res.status(200).send({ data, msg: `Action removed` })
    },
}
