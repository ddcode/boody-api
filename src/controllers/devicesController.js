import Device from '../models/Device'

export default {
    async findAll(req, res, next) {
        const data = await Device.find()

        return res.status(200).send(data)
    },

    async findOneById(req, res, next) {
        const { id } = req.params

        const data = await Device.findOne({
            _id: id,
        }).populate('location')
        if (!data) next()

        res.status(200).send(data)
    },

    async create(req, res, next) {
        const data = await new Device(req.body).save()

        return res.status(200).send({ data, msg: `Device created` })
    },

    async update(req, res, next) {
        const { id } = req.params
        const data = await Device.findOne({ _id: id })

        const record = Object.assign(data, req.body)

        record.save()

        return res.status(200).send({ record, msg: `Device updated` })
    },

    async remove(req, res, next) {
        const { id } = req.params

        await Device.remove({ _id: id })
        const data = await Device.find()

        return res.status(200).send({ data, msg: `Device removed` })
    },
}
