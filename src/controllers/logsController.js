import Log from '../models/Log'

export default {
    async findAll(req, res, next) {
        const data = await Log.find()
            .populate('action')
            .populate('device')
            .populate('location')

        return res.status(200).send({ data })
    },
}
