import Location from '../models/Location'

export default {
    async findAll(req, res, next) {
        const data = await Location.find()

        return res.status(200).send(data)
    },

    async findOneById(req, res, next) {
        const { id } = req.params

        const data = await Location.findOne({
            _id: id,
        })
        if (!data) next()

        res.status(200).send(data)
    },

    async create(req, res, next) {
        const data = new Location(req.body).save()

        return res.status(200).send(data)
    },

    async update(req, res, next) {
        const { id } = req.params
        const data = await Location.findOne({ _id: id })

        const record = Object.assign(data, req.body)

        record.save()

        return res.status(200).send(data)
    },

    async remove(req, res, next) {
        const { id } = req.params

        await Location.remove({ _id: id })
        const data = await Location.find()

        return res.status(200).send({ data, msg: `Location removed` })
    },
}
