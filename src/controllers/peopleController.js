import People from '../models/People'

export default {
    async findAll(req, res, next) {
        const data = await People.find()

        return res.status(200).send(data)
    },

    async findOneById(req, res, next) {
        const { id } = req.params

        const data = await People.findOne({
            _id: id,
        })
        if (!data) next()

        res.status(200).send(data)
    },

    async create(req, res, next) {
        const data = new People(req.body).save()

        return res.status(200).send(data)
    },

    async update(req, res, next) {
        const { id } = req.params
        const { name } = req.body
        const data = await People.findOne({ _id: id })

        data.name = name
        data.save()

        return res.status(200).send(data)
    },
}
